FROM openjdk:18-slim-buster

ARG KICODIA_JAR_URL=https://rtsys.informatik.uni-kiel.de/~kieler/files/nightly/sccharts/cli/kicodia.linux.jar

ADD ${KICODIA_JAR_URL} /app/kicodia.linux.jar
ADD entrypoint.sh /app/entrypoint.sh

RUN apt-get update &&\
    apt-get install -y xvfb dbus-x11 libswt-gtk-4-java libswt-gtk-4-jni &&\
    apt-get clean

ENTRYPOINT ["/bin/bash", "/app/entrypoint.sh"]