#!/bin/bash

export DISPLAY=:99

Xvfb -ac -listen tcp $DISPLAY &

function kicodia() { java -Xmx512m --add-opens java.base/java.lang=ALL-UNNAMED --add-opens java.base/jdk.internal.loader=ALL-UNNAMED -jar /app/kicodia.linux.jar "$@"; }

if [[ "$1" = "-" || "$1" = "diagram" ]]; then
    shift
    IN=$(mktemp -d)
    cat > ${IN}/scchart.sctx
    [ -s ${IN}/scchart.sctx ] &&
	kicodia --only-diagram "$@" ${IN} > /dev/null 2>&1 &&
	cat ${IN}/scchart.png
else
    kicodia "$@"
fi

