# Kicodia

Container image to run the command-line interface of the Kieler Compiler, a modeling toolkit by the [Real-Time and Embedded Systems Work Group](http://www.informatik.uni-kiel.de/en/rtsys/) of the Department of Computer Science of the Christian-Albrechts University of Kiel.

The image includes a Java 1.8 JDK and an X11 virtual frame buffer.

## General case

The entrypoint will run the `kicodia` command line:

```
docker run -i kicodia --help
docker run -i -v$PWD:/run kicodia -d -g /run/output /run
```

## Compiling a diagram only

As a special case, if the first argument is `-` or `diagram`, an SSChart file will be read from stdin, converted to a diagram only, and sent to stdout:

```
docker run -i kicodia - < examples/simple.sctx > simple.png
```
